#! /usr/bin/env python

import sys
import re
import random

wordlist = ['english-words.10', 'english-words.20']

words = []
for wl in wordlist:
    words += file(wl).readlines()

skipped = '_auyieo()[]{}"\''
allowed = ' zxcvbnmasdfghjklqwrtp023456789.'
letters = 'zxcvbnmasdfghjklqwertyuiop'
words = [
        (
            word.lower().strip(),
            ''.join([c for c in word.lower().strip() if c not in skipped])
        )
        for word in words if len(word.strip()) > 1 and word.strip()[-1] in letters]

oneletters = 'a'

mode = sys.argv[1]
sentence = True

def encode(data):
    bits = ''
    for c in data:
        index = allowed.find(c)
        s = bin(index)[2:]
        if len(s) < 5:
            s = '0' * (5 - len(s)) + s
        bits += s
    if len(bits) % 8:
        bits += '0' * (8 - (len(bits) % 8))
    out = ''
    for i in range(len(bits) / 8):
        out += chr(int(bits[i * 8:i * 8 + 8], 2))
    return out

def decode(data):
    bits = ''
    for c in data:
        s = bin(ord(c))[2:]
        if len(s) < 8:
            s = '0' * (8 - len(s)) + s
        bits += s
    out = ''
    for i in range(len(bits) / 5):
        out += allowed[int(bits[i * 5:i * 5 + 5], 2)]
    return out

def reconstruct(word):
    global sentence
    if word == '.':
        return 'a'
    end = False
    cap = False
    if sentence:
        cap = True
        sentence = False
    if word.endswith('.'):
        word = word[:-1]
        end = True
    pick = [w[0] for w in words if w[1] == word]
    if len(pick):
        word = random.choice(pick)
    if end:
        sentence = True
        word += '.'
    if cap:
        word = word.capitalize()
    return word

for arg in sys.argv[2:]:
    if mode in ['-c', '--compress']:
        data = re.sub(r'[\t\n\r -]+', ' ', file(arg).read().lower())
        data = re.sub(r'[\?\!\:\;\,]+', '.', data)
        data = re.sub(r'1', 'l', data)
        data = re.sub(r'\.+', '.', data)
        new = ''
        for i in range(len(data)):
            try:
                lastChar = data[i - 1]
            except:
                lastChar = '\x00'
            try:
                nextChar = data[i + 1]
            except:
                nextChar = '\x00'
            if lastChar == ' ' and nextChar == ' ':
                if data[i] in skipped:
                    if data[i] == 'i':
                        new += 'l'
                    else:
                        new += '.'
                else:
                    new += data[i]
            elif data[i] not in skipped:
                new += data[i]
        data = new
        data = ''.join([c for c in data if c in allowed])
        data = encode(data)
        file(arg + '.cmp', 'w+').write(data.strip())
    elif mode in ['-d', '--decompress']:
        data = decode(file(arg).read())
        data = re.sub(r' l ', ' I ', data)
        data = ' '.join([reconstruct(w) for w in data.split()])
        print data
